package constructj;

public class MemberThis {
    String nameField;

    void initMember(String nameParameter) {
        nameField = nameParameter;
    }
}

//매개변수와 멤버변수의 명칭이 다르면 this 생략 가능
//매개변수에서 우선 찾고 없으면 멤버변수로가기 때문 (java에서 알아서 찾음)
