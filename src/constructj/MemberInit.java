package constructj;

public class MemberInit {
    String name;
    int age;
    int grade;

    //초기화 메서드 추가
    void initMember(String name, int age, int grade) {
        this.name = name; //메서드의 매개변수와 멤버변수의 이름이 똑같을때 멤버변수에 this를 붙여줘야 함 : this 만들어진 인스턴스를 가리킴
        this.age = age;
        this.grade = grade;
    }
}

//initMember의 매개변수의 같은 코드블럭 안에서 선언되어 있기때문에 우선순위를 가짐
//코드 안에서 name을 사용하면 매개변수를 우선적으로 바라보게 됨
//멤버 변수에 접근하기 위해 this 사용
//this : 만들어진 인스턴스 자신을 참조

