package constructj;

public class MemberDefault {
    String name;

    //기본생성자를 직접 만들수도 있음
    MemberDefault(){
        System.out.println("생성자 호출");
    }
}

//기본생성자 : 매개변수가 없는 생성자
//매개변수가 하나도 없으면 java가 알아서 기본 생성자를 만듬
//생성자가 있으면 기본생성자 x
