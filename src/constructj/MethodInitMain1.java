package constructj;

public class MethodInitMain1 {

    public static void main(String[] args) {
        MemberInit member1 = new MemberInit();
        member1.name = "user1";
        member1.age = 15;
        member1.grade = 90;

        MemberInit member2 = new MemberInit();
        member2.name = "user2";
        member2.age = 16;
        member2.grade = 80;

        MemberInit member3 = new MemberInit();
        member3.name = "user3";
        member3.age = 17;
        member3.grade = 100;

        MemberInit[] members = {member1, member2, member3};

        for (MemberInit s : members) {
            System.out.println("이름:" + s.name + " 나이:" + s.age + " 성적:" + s.grade);
        }
    }
}

//객체(인스턴스)를 생성하고 나면 초기값을 설정해줘야 한다