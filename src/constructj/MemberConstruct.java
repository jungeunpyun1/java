package constructj;

//생성자 : 객체를 생성과 동시에 초기값을 할당해야하는 경우 사용
public class MemberConstruct {
    String name;
    int age;
    int grade;

    //overloading 추가
    //이름과 나이만 있으면 성적은 50
    //생성자 2개
//    MemberConstruct(String name, int age) {
//        this.name;
//        this.age;
//        this.grade = 50;
//    }

    //생성자 중복 없애는 방법
    //this() : 자기자신의 생성자 호출, 생성자 안에서만 쓸 수 있음
    //생성자 코드의 첫줄에서만 작성할 수 있음!!!!!!
    MemberConstruct(String name, int age) {
        this(name, age, 50); //밑에있는 생성자 호출
    }

    //생성자는 클래스명과 똑같아야 한다!!!!
    MemberConstruct(String name, int age, int grade) {
        System.out.println("생성자 호출 name=" + name + ",age=" + age + ",grade=" + grade);
        this.name = name; //멤버변수와 이름이 같으므로 this 필요
        this.age = age;
        this.grade = grade;
    }
}

//생성자와 메서드의 차이
//클래스명과 일치!
//반환타입 없음 (메서드는 void같은 반환 타입이 있지만 생성자는 반환타입이 없다)