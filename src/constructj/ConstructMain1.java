package constructj;

public class ConstructMain1 {
    public static void main(String[] args) {
        MemberConstruct member1 = new MemberConstruct("user1", 15, 90);
        MemberConstruct member2 = new MemberConstruct("user2", 16, 80);
        //MemberConstruct member3 = new MemberConstruct();
        //에러발생 : 생성자가 없으면 에러발생함
        //한번에 이루어지는 것처럼 보이지만 실제로는 2step : 메모리에 인스턴스 생성, 즉시 생성자 호출

        MemberConstruct[] members = {member1, member2};

        for (MemberConstruct s : members) {
            System.out.println("이름:" + s.name + " 나이:" + s.age + " 성적:" + s.grade);
        }
    }
}

//생성자 활용의 장점
//인스턴스 생성 후 초기값 설정 하는 두 단계를 하나로 줄일 수 있음
//실수로 초기값 설정을 하지 않는 경우를 방지할 수 있다. 데이터가 없어도 에러가 발생하진 않기 때문에 유령데이터가 만들어짐
//객체를 생성할때 직접 정의한 생성자가 있으면 초기값을 생성하지 않는경우 컴파일 오류가 발생함 : 에러 확인 용이
//생성자를 사용하면 필수값 입력을 보장할 수 있다!!!
//유령회원이 나올 가능성을 원천적으로 차단함!!!!!!!!!
//보통 필수값을 생성자에서 받고 추가정보를 따로 받음
