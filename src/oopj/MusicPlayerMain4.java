package oopj;

public class MusicPlayerMain4 {

    public static void main(String[] args) {
        MusicPlayer player = new MusicPlayer();
        //음악 플레이어 켜기
        player.on();
        //볼륨 증가
        player.volumeUp();
        //볼륨 증가
        player.volumeUp();
        //볼륨 감소
        player.volumeDown();
        //음악 플레이어 상태
        player.showStatus();
        //음악 플레이어 끄기
        player.off();
    }
}

//클래스를 만들지 않은 개발자가 음악플레이어 동작 함수를 손쉽게 만들 수 있다
//필요한 기능만 호출하면 됨, 클래스 내부는 알 필요 없음
//캡슐화 : 모든 속성과 기능이 하나의 캡슐(클래스)에 묶여져 있는 것
//뮤직플레이어의 기능이 변경될때 클래스 내부의 메서드만 변경하면 됨
