package oopj;

public class ValueData {
    //속성
    int value;

    //메서드 : 메서드는 객체를 생성해야 호출할 수 있음
    void add() {
        value++;
        System.out.println("숫자 증가 value=" + value);
    }
}

//데이터와 메서드 함께 정의한 클래스