package oopj;

//반복되는 동작은 메서드로 분리할 수 있음 : 각각의 기능을 모듈화한것
//메서드 분리(모듈화)의 이점
//1.중복 제거 : 메서드를 뽑으면 메인 로직이 깔끔해짐
//2.변경 영향 범위 제한 (디버깅할 부분이 제한됨)
//3.메서드 이름 추가 : 사람이 이해하기에 더욱 쉬워짐

public class MusicPlayerMain3 {

    public static void main(String[] args) {
        MusicPlayerData data = new MusicPlayerData(); //data : x001
        //모두 같은 인스턴스의 멤버 변수값을 변경하는 것
        //음악 플레이어 켜기
        on(data);
        //볼륨 증가
        volumeUp(data);
        //볼륨 증가
        volumeUp(data);
        //볼륨 감소
        volumeDown(data);
        //음악 플레이어 상태
        showStatus(data);
        //음악 플레이어 끄기
        off(data);
    }

    static void on(MusicPlayerData data) {
        data.isOn = true; //x001.isOn = true
        System.out.println("음악 플레이어를 시작합니다");
    }

    static void off(MusicPlayerData data) {
        data.isOn = false;
        System.out.println("음악 플레이어를 종료합니다.");
    }

    static void volumeUp(MusicPlayerData data) {
        data.volume++;
        System.out.println("음악 플레이어 볼륨:" + data.volume);
    }

    static void volumeDown(MusicPlayerData data) {
        data.volume--;
        System.out.println("음악 플레이어 볼륨:" + data.volume);
    }

    static void showStatus(MusicPlayerData data) {
        System.out.println("음악 플레이어 상태 확인");
        if (data.isOn) {
            System.out.println("음악 플레이어 ON, 볼륨:" + data.volume);
        } else {
            System.out.println("음악 플레이어 OFF");
        }
    }
}

//이것도 결국 잘 만든 절차지향 프로그래밍임
//절차지향의 한계 : 데이터와 기능이 분리되어 있음
//데이터는 클래스안에 있지만, 기능은 메인 로직이 있는 코드에 있음
//데이터는 클래스를 사용해야하고 기능은 다른 코드안에 있는 메서드를 사용해야함
//기능은 모두 데이터를 사용하기 때문에 데이터와 기능은 모두 밀접하게 연계되어 있음
//데이터가 변경되면 해당 데이터를 사용하는 메서드들도 찾아서 모두 변경해야함 (디버깅의 어려움)
