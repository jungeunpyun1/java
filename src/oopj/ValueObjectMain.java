package oopj;


public class ValueObjectMain {

    public static void main(String[] args) {
        ValueData valueData = new ValueData(); //x002
        valueData.add(); //x002.add() 실행 - 클래스의 메서드를 호출해서 사용, 기본적으로 본인 인스턴스 내의 value에 접근
        valueData.add();
        valueData.add();
        System.out.println("최종 숫자=" + valueData.value);
    }

}

//클래스는 속성(멤버변수)과 기능(메서드) 모두 정의 가능
//내가 나의 데이터를 사용하기 때문에 더욱 응집력이 높다 (속성과 기능이 뭉쳐져있음)