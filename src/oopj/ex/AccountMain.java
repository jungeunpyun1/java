package oopj.ex;

public class AccountMain {

    public static void main(String[] args) {
        Account account = new Account();
        account.deposit(10000);
        account.withdraw(9000);
        account.withdraw(2000); //오류 메시지 출력
        System.out.println("잔고: " + account.balance);
    }
}


//마무리 정리
//객체 지향도 결국 순서대로 작동하는 것은 마찬가지
//다만 절차지향은 속성(데이터)와 기능(메서드)이 분리되어 있고 - 어떻게 동작하는지!
//객체지향은 속성과 기능이 캡슐화되어 있다는 것이 포인트! - 무엇을 모듈화 하는지!
//절차지향은 데이터와 해당 데이터에 대한 기능이 분리되어 있음
//객체지향은 데이터와 그 데이터에 대한 기능히 합쳐져 있음 (하나의 인스턴스 내에 있는 데이터를 활용해서 기능을 수행)
//세상의 모든 사물은 속성과 기능으로 나누어져 있음 : 객체화 가능

