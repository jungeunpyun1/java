package oopj;

//객체지향 : 객체를 중심으로, 순서보다 "무엇을" 중심으로 프로그래밍
//음악플레이어라는 개념을 하나의 객체로 온전히 만드는것이 중요
//필요한 변수 : 상태값, 볼륨
//필요한 기능 : 켜기, 끄기, 소리up, 소리down, 상태체크
//객체는 실제 세상에 존재하는 사물을 가리키기도 함
//세상의 모든 사물은 속성과 기능으로 이루어져 있음, 그것을 모두 클래스에 표현해줘야 함

public class MusicPlayer {

    int volume = 0;
    boolean isOn = false;

    //메서드에는 static 붙지 않음
    //메서드 안에 있는 변수는 자신의 멤버 변수
    void on() {
        isOn = true;
        System.out.println("음악 플레이어를 시작합니다");
    }

    void off() {
        isOn = false;
        System.out.println("음악 플레이어를 종료합니다.");
    }

    void volumeUp() {
        volume++;
        System.out.println("음악 플레이어 볼륨:" + volume);
    }

    void volumeDown() {
        volume--;
        System.out.println("음악 플레이어 볼륨:" + volume);
    }

    void showStatus() {
        System.out.println("음악 플레이어 상태 확인");
        if (isOn) {
            System.out.println("음악 플레이어 ON, 볼륨:" + volume);
        } else {
            System.out.println("음악 플레이어 OFF");
        }
    }

}

//음악 플레이어에 필요한 모든 속성과 기능을 하나의 클래스에 모두 정의 완료