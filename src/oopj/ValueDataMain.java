package oopj;

public class ValueDataMain {

    public static void main(String[] args) {
        ValueData valueData = new ValueData(); //x001
        add(valueData);
        add(valueData);
        add(valueData);
        System.out.println("최종 숫자=" + valueData.value);
    }

    //값을 1씩 증가시키는 메서드
    static void add(ValueData valueData) {
        valueData.value++; //value 초기값 0
        System.out.println("숫자 증가 value=" + valueData.value);
    }
}

//이 코드도 마찬가지로 절차지향

//객체지향언어는 클래스 내부에 속성(데이터)와 기능(메서드)를 함께 포함할 수 있음!!
